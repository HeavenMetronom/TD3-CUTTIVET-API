package hu.pe.anthonycuttivet.td3_api;

import java.util.List;

import retrofit.http.GET;
import retrofit2.Call;
import retrofit2.http.Path;

/**
 * Created by Anthony C on 20/11/2017.
 */

public interface GitHubService {
    @GET("users/{user}/repos")
    Call<List<Repo>> listRepos(@Path("user") String user);
}
